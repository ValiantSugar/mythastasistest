using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//����� ��������� �������� ����� �������� � ��� ����� ���� �� ��������� ������� �� 3 ����� ��� ���������������, �� ��� ������� ��������� ������ ��� �� ��������������
public class Closet : MonoBehaviour
{
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private Image _teethImage;
    [SerializeField] private float _openedPositionY;
    [SerializeField] private float _middlePositionY;
    [SerializeField] private float _closedPositionY;
    [SerializeField] private float _openCloseTime;
    [SerializeField] private Button[] _tagButtons;
    [SerializeField] private CanvasGroup[] _tagGroups;

    private bool _isGrabed;
    private Vector3 _offset;

    private CanvasGroup _currentGroup;
    private Sequence _groupSequence;

    private Coroutine _openCloseCoroutine;

    private Action<float> OnPositionYChanged;

    private void Awake()
    {
        OnPositionYChanged += ChangeTeethAlpha;

        for (var i = 0; i < _tagButtons.Length; i++)
        {
            var index = i;
            _tagButtons[index].onClick.AddListener(() => OpenGroup(index));
        }

        OpenGroup(0);
    }

    private void Start()
    {
        GrabItemHelper.Instance.OnCurrentItemChanged += OnItemGrabed;
    }

    private void Update()
    {
        if (!_isGrabed)
            return;

        var newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + _offset;

        SetNewPosition(newPosition);
    }

    private void OnItemGrabed(bool flag)
    {
        _currentGroup.GetComponent<ScrollRect>().enabled = !flag;
    }

    private void OpenGroup(int index)
    {
        var choosenGroup = _tagGroups[index];
        if (_currentGroup == choosenGroup)
            return;

        _tagButtons[index].transform.parent.SetAsLastSibling();

        if (_groupSequence != null)
            _groupSequence.Kill();

        _groupSequence = DOTween.Sequence();

        if (_currentGroup != null)
        {
            var previousGroup = _currentGroup;
            _groupSequence.Insert(0, previousGroup.DOFade(0, 0.5f).OnComplete(() => previousGroup.gameObject.SetActive(false)));
        }

        _currentGroup = choosenGroup;

        choosenGroup.gameObject.SetActive(true);

        _groupSequence.Insert(0, choosenGroup.DOFade(1, 0.5f));
        _groupSequence.Play();
    }

    private void SetNewPosition(Vector3 position)
    {
        if (position.y > _openedPositionY)
            position.y = _openedPositionY;
        else if (position.y < _closedPositionY)
            position.y = _closedPositionY;

        var newPosition = new Vector3(_rectTransform.position.x, position.y, _rectTransform.position.z);
        _rectTransform.position = newPosition;
        OnPositionYChanged.Invoke(_rectTransform.position.y);
    }

    private IEnumerator OpenCoroutine()
    {
        var lastY = _rectTransform.position.y;
        var timer = 0f;
        var distPerTimeUnit = _openCloseTime / (_openedPositionY - _closedPositionY);
        var diff = _openedPositionY - lastY;
        if (diff == 0)
            yield break;

        var increaseTimeUnit = 1 / (diff * distPerTimeUnit);

        while (timer < 1f)
        {
            var newY = Mathf.Lerp(lastY, _openedPositionY, timer);
            timer += increaseTimeUnit * Time.deltaTime;
            var newPosition = new Vector3(_rectTransform.position.x, newY, _rectTransform.position.z);
            _rectTransform.position = newPosition;
            OnPositionYChanged.Invoke(newY);
            yield return new WaitForEndOfFrame();
        }

        _rectTransform.position = new Vector3(_rectTransform.position.x, _openedPositionY, _rectTransform.position.z);
        OnPositionYChanged.Invoke(_openedPositionY);
    }

    private IEnumerator CloseCoroutine()
    {
        var lastY = _rectTransform.position.y;
        var timer = 0f;
        var distPerTimeUnit = _openCloseTime / (_openedPositionY - _closedPositionY);
        var diff = lastY - _closedPositionY;
        if (diff == 0)
            yield break;

        var increaseTimeUnit = 1 / (diff * distPerTimeUnit);

        while (timer < 1f)
        {
            var newY = Mathf.Lerp(lastY, _closedPositionY, timer);
            timer += increaseTimeUnit * Time.deltaTime;
            var newPosition = new Vector3(_rectTransform.position.x, newY, _rectTransform.position.z);
            _rectTransform.position = newPosition;
            OnPositionYChanged.Invoke(newY);
            yield return new WaitForEndOfFrame();
        }

        _rectTransform.position = new Vector3(_rectTransform.position.x, _closedPositionY, _rectTransform.position.z);
        OnPositionYChanged.Invoke(_closedPositionY);
    }

    private void ChangeTeethAlpha(float positionY)
    {
        var distDiff = _openedPositionY - _closedPositionY;
        var currentDist = _openedPositionY - _rectTransform.position.y;
        var tempColor = _teethImage.color;
        tempColor.a = (1f / distDiff) * currentDist;
        _teethImage.color = tempColor;
        if (tempColor.a < 0.05f)
            _teethImage.gameObject.SetActive(false);
        else
            _teethImage.gameObject.SetActive(true);
    }

    public void OnPointerDown(Vector2 mousePositionByUI)
    {
        _isGrabed = true;
        if (_openCloseCoroutine != null)
            StopCoroutine(_openCloseCoroutine);

        _offset = _rectTransform.position - Camera.main.ScreenToWorldPoint(mousePositionByUI);
    }

    public void OnPointerUp()
    {
        _isGrabed = false;
        if (_openCloseCoroutine != null)
            StopCoroutine(_openCloseCoroutine);

        if (_rectTransform.position.y > _middlePositionY)
            _openCloseCoroutine = StartCoroutine(OpenCoroutine());
        else
            _openCloseCoroutine = StartCoroutine(CloseCoroutine());
    }
}
