using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabItemHelper : MonoBehaviour
{
    public static GrabItemHelper Instance;

    [SerializeField] private SpriteRenderer _itemSprite;

    private ItemBase _currentItem;
    private Vector3 _cursorOffset;

    public ItemBase enterItem;
    public ItemsHolder enterItemHolder;

    public Action<bool> OnCurrentItemChanged;

    private void Awake()
    {
        Instance = this; //�� ���� ������ ������� ����� � ������ ����� ���������
    }

    //TODO: ��� ������ ���������� ���������� Update - ����� ������� Juggler, ����� �� ������� ��� �������
    private void Update()
    {
        if (_currentItem == null)
            return;

        var newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + _cursorOffset;
        _itemSprite.transform.position = new Vector3(newPosition.x, newPosition.y, _itemSprite.transform.position.z);

        if (!Input.GetMouseButtonUp(0))
            return;

        RemoveItem();
    }

    public void SetItem(ItemBase item, bool withOffset)
    {
        _currentItem = item;
        _itemSprite.sprite = _currentItem.GetSpriteForCursor();
        _itemSprite.enabled = true;

        if (withOffset)
            _cursorOffset = _currentItem.transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        else
            _cursorOffset = Vector3.zero;

        if (OnCurrentItemChanged != null) //�������� ����� ? ����� ��������� ������������, ��� ��� ��������� ����� if
            OnCurrentItemChanged.Invoke(true);
    }

    public void RemoveItem()
    {
        if (_currentItem != null)
        {
            if (enterItem != null)
                enterItem.ReturnToBase(_currentItem.itemData);
            else if (enterItemHolder != null)
                enterItemHolder.AddItem(_currentItem.itemData);
            else
                _currentItem.ReturnToBase(_currentItem.itemData);
        }

        _currentItem = null;

        _itemSprite.sprite = null;
        _itemSprite.enabled = false;

        if (OnCurrentItemChanged != null) //�������� ����� ? ����� ��������� ������������, ��� ��� ��������� ����� if
            OnCurrentItemChanged.Invoke(false);
    }

    public bool CheckIsSuitableItem(ItemType itemType, int itemIndex = 0)
    {
        if (_currentItem == null || _currentItem.itemData.itemType != itemType)
            return false;

        if (itemIndex != 0 && _currentItem.itemData.itemIndex != itemIndex)
            return false;

        return true;
    }

    public bool CheckIsSuitableItems(ItemType itemType, ItemsHolder.ItemContainer[] itemsContainers)
    {
        if (_currentItem == null || _currentItem.itemData.itemType != itemType)
            return false;

        //TODO: ���������� �� �����
        foreach (var container in itemsContainers)
        {
            if (_currentItem.itemData.itemIndex == container.index)
                return true;

        }

        return false;
    }
}
