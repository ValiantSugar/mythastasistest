using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemInCloset : ItemBase
{
    [SerializeField] private Image _image;

    private bool _isTaken;

    public override void ReturnToBase(ItemScriptableObject item)
    {
        _isTaken = false;
        var tempColor = _image.color;
        tempColor.a = 1f;
        _image.color = tempColor;
    }

    public override void RemoveSprite()
    {
        _isTaken = true;
        var tempColor = _image.color;
        tempColor.a = 0f;
        _image.color = tempColor;
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (GrabItemHelper.Instance.CheckIsSuitableItem(itemData.itemType, itemData.itemIndex))
            GrabItemHelper.Instance.enterItem = this;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        GrabItemHelper.Instance.enterItem = null;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (_isTaken)
            return;

        GrabItemHelper.Instance.SetItem(this, true);
        RemoveSprite();
    }
}
