using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemOnCharacter : ItemBase
{
    [SerializeField] private ItemType _needlyType;
    [SerializeField] private GameObject[] _itemPrefabs;

    private GameObject _currentItem;

    public override void ReturnToBase(ItemScriptableObject item)
    {
        this.itemData = item;
        _currentItem = Instantiate(item._characterPrefab, transform);
    }

    public override void RemoveSprite()
    {
        Destroy(_currentItem);
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (_currentItem == null && GrabItemHelper.Instance.CheckIsSuitableItem(_needlyType))
            GrabItemHelper.Instance.enterItem = this;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        GrabItemHelper.Instance.enterItem = null;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (_currentItem == null)
            return;

        GrabItemHelper.Instance.enterItem = null;
        GrabItemHelper.Instance.SetItem(this, false);
        RemoveSprite();
    }
}
