using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemsHolder : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Serializable] 
    public class ItemContainer
    {
        public int index;
        public Image image; //TODO: ������ ���������� �� 2 "�������" ����� �������������
        public ItemBase item;
    }

    [SerializeField] private ItemType _itemType;
    [SerializeField] private ItemCompoundInCloset _itemCompoundInCloset;
    [SerializeField] private ItemContainer[] _itemContainers;

    public void AddItem(ItemScriptableObject itemData)
    {
        var checkActiveCount = 0;
        foreach (var container in _itemContainers)
        {
            if (container.image != null && container.image.enabled)
            {
                checkActiveCount++;
                continue;
            }

            if (container.index != itemData.itemIndex)
                continue;

            //container.image.sprite = itemData._closetSprite;
            if (container.image != null)
                container.image.enabled = true;
            else
                container.item.ReturnToBase(null);
            checkActiveCount++;
        }

        if (checkActiveCount >= _itemContainers.Length && _itemCompoundInCloset != null)
            _itemCompoundInCloset.SetCanBeTaken();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GrabItemHelper.Instance.CheckIsSuitableItems(_itemType, _itemContainers))
            GrabItemHelper.Instance.enterItemHolder = this;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GrabItemHelper.Instance.enterItemHolder = null;
    }
}
