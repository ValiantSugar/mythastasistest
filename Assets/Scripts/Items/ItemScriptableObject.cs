using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
public class ItemScriptableObject : ScriptableObject
{
    public ItemType itemType;
    public int itemIndex;
    public Sprite _closetSprite;
    public Sprite _cursorSprite;
    public GameObject _characterPrefab;
}
