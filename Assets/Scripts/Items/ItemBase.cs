using UnityEngine;
using UnityEngine.EventSystems;

public abstract class ItemBase : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    public ItemScriptableObject itemData;

    public Sprite GetSpriteForCursor()
    {
        if (itemData == null)
            return null;

        return itemData._cursorSprite;
    }

    public abstract void ReturnToBase(ItemScriptableObject item);

    public abstract void RemoveSprite();

    public abstract void OnPointerEnter(PointerEventData eventData);

    public abstract void OnPointerExit(PointerEventData eventData);

    public abstract void OnPointerDown(PointerEventData eventData);
}
