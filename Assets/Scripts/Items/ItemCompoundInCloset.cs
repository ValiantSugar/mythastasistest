using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemCompoundInCloset : ItemInCloset
{
    [SerializeField] private CanvasGroup _canvasGroup;

    private bool _isCanBeTaken;

    public override void ReturnToBase(ItemScriptableObject item)
    {
        base.ReturnToBase(item);
        _canvasGroup.alpha = 1f;
    }

    public override void RemoveSprite()
    {
        base.RemoveSprite();
        _canvasGroup.alpha = 0f;
    }

    public void SetCanBeTaken()
    {
        _isCanBeTaken = true;
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (_isCanBeTaken)
            base.OnPointerEnter(eventData);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        if (_isCanBeTaken)
            base.OnPointerExit(eventData);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (_isCanBeTaken)
            base.OnPointerDown(eventData);
    }
}
