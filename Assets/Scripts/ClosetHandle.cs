using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClosetHandle : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Closet _closet;

    public void OnPointerDown(PointerEventData eventData)
    {
        _closet.OnPointerDown(eventData.pressPosition);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _closet.OnPointerUp();
    }
}
